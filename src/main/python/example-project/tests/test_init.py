from unittest import TestCase
from unittest.mock import MagicMock

class InitTest(TestCase):

    def test_import(self):
        import example_package

    def test_output(self):
        import example_package
        method = MagicMock()
        example_package.main(method)
        method.assert_called_once_with(
            example_package.__name__,
            example_package.__version__,
        )