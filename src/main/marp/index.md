---
marp: true
theme: uncover
---

<style>
section {
    text-align: left;
    font-size: 28px;
}
ul {
    margin-left: 0px;
}
section.center p, section.center ul, section.center li,
section.center h1, section.center h2, section.center h3,
section.center h4, section.center h5, section.center h6  {
    text-align: center;
}
section h1 {
    font-size: 40px;
}
section::after {
  font-weight: bold;
}
section.cover header{
    text-align: right;
    opacity: 0.1;
}
section pre {
    margin: inherit;
    margin-bottom: 15px;
}
section.center pre {
    margin: 0 auto;
    margin-bottom: 15px;
}
</style>

<!-- _class: center invert cover -->

<style scoped>
    section h1 {
        font-size: 55px;
    }
    footer {
        text-align: right;
    }
</style>

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->

<!-- _footer: Powered by [Marp](https://yhatt.github.io/marp/) & [PlantUML](https://github.com/plantuml) -->

# PyPI: Publicando seu primeiro pacote Python
Ettore Leandro Tognoli


![](resources/flisol.svg)

---

Código fonte dos slides disponível

![bg right fit](resources/qrcode.svg)

<!-- _footer: https://gitlab.com/ettoreleandrotognoli/pypi-flisol -->

---

<style scoped>
p {
    font-size: 18px
}
</style>

# Sobre mim

### Ettore Leandro Tognoli

Eng. Software - Red Hat

Mestre em Ciência da Computação - UFSCar

Bacharel em Ciência da Computação - UNIVEM

![bg right fit](resources/me.jpg)

<!-- _footer: https://github.com/ettoreleandrotognoli -->

---
# Agenda

- Introdução
    - Python
    - PyPI
    - PIP
- Projeto
    - Gerenciamento de Dependências
    - Testes
    - Pacote
    - Publicação

---

<!-- paginate: true -->

<!-- header: Introdução: Python -->

<!-- _class: center -->

![](resources/python-logo.svg)

_Python é uma linguagem de programação de alto nível, interpretada de script, imperativa, orientada a objetos, funcional, de tipagem dinâmica e forte. Foi lançada por Guido van Rossum em 1991._



<!-- _footer: https://www.python.org/ -->

---

<!-- _class: center -->
# Python 2

```py
# -*- coding: utf-8 -*-
print 'Hello World!'
print xrange(10)

def yield_from():
    for i in range(10):
        yield i
```

Python 2 foi descontinuado em 2020, sua última versão foi a [2.7.18](https://www.python.org/downloads/release/python-2718/) lançada em Abril de 2020

<!-- _footer: https://www.python.org/doc/sunset-python-2/ -->

---
<!-- _class: center -->
# Python 3

```py
print('Hello World!')
print(range(10))

def yield_from():
    yield from range(10)

```

---

<!-- _class: center invert -->

# Python 3

Usaremos somente python 3, então verifique a versão que você tem instalado e se necessário instale a versão correta.

```sh
~$ dnf install python3 python3-pip
```

```sh
~$ apt-get install python3 python3-pip
```


```sh
~$ apk add python3 py3-pip
```


<!-- _footer: https://www.python.org/downloads/ -->

---

# Módulo Python

```sh
~$ python -m http.server
```

<!-- _footer: https://github.com/python/cpython/blob/master/Lib/http/server.py -->

---

<!-- header: Introdução: PyPI -->

# PyPI

The Python Package Index

_PyPI helps you find and install software developed and shared by the Python community._
_Package authors use PyPI to distribute their software._

![bg right 80%](resources/pypi-logo.svg)

<!-- _footer: https://pypi.org/ -->

---

<!-- header: Introdução: PIP -->

# PIP

The Python Package Installer

PIP Install Pacakges

PIP Install Python

<!-- _footer: https://pip.pypa.io/en/stable/ -->


---

# Exemplo com youtube-dl

Pesquisa no PyPI: https://pypi.org/search/?q=youtube-dl
Página no PyPI: https://pypi.org/project/youtube_dl/

Instalação:
```sh
~$ pip install --user youtube-dl
```

Uso:
```sh
~$ youtube-dl <url>
```

<!-- _footer: https://github.com/ytdl-org/youtube-dl -->

---
# --user ?

Seu S.O. pode estar usando o python padrão, instalar coisas sem `--user` pode gerar algum conflito e geralmente requer permissões especiais.

Pacotes do usuário:
```sh
~$ pip freeze --user
...
youtube_dl==2021.4.7
...
```

Pacotes do S.O.:
```sh
~$ pip freeze
...
...
...
```

---

<!-- _class: invert -->

<!-- header: Projeto -->

# Iniciando nosso projeto

---

<!-- header: Projeto: Gerenciamento de Dependências -->

# Gerenciamento de Dependências

Algumas opções:

[virtualenv](https://docs.python.org/pt-br/3/library/venv.html)
[poetry](https://python-poetry.org/)
[conda](https://docs.conda.io/en/latest/)
[pipenv](https://github.com/pypa/pipenv)

---

# pipenv

Instalar pipenv e iniciar projeto:

```sh
~$ python -m pip install --user pipenv
~$ mkdir example-project
~$ cd example-project
~$ pipenv install
```

Resultado:

- example-project
    - Pipfile
    - Pipfile.lock

---

# Dependências


- Runtime:

    Dependências necessárias para funcionar na máquina do "cliente"
    Exemplos: 

- Dev time:

    Dependências necessárias somente durante o desenvolvimento.
    Exemplos: formatação de código, testes, cobertura, publicação

---

# Nossas dependências:

- [coverage](https://coverage.readthedocs.io/en/coverage-5.5/)

    Usado para fazer testes de cobertura

- [twine](https://pypi.org/project/twine/)

    Usado para publicar o pacote no PyPI

```
~$ pipenv install --dev coverage twine
```

Mudanças em Pipfile e Pipfile.lock

---

<!-- header: Projeto: Código -->


Criar pacote ( `__init__.py` ) e `__main__.py`

```sh
~$ mkdir example_package
~$ touch example_package/__init__.py
~$ touch example_package/__main__.py
```

---

<!-- embedme src/main/python/example-project/example_package/__init__.py -->

example_package/\_\_init__.py

```py
__version__ = '0.0.0'

def main(output=print):
    output(__name__, __version__)
```

---


<!-- embedme src/main/python/example-project/example_package/__main__.py -->

example_package/\_\_main__.py

```py
from example_package import main

if __name__ == '__main__':
    main()

```

---

<!-- header: Projeto: Testes -->

# Testes

Criar pacote de testes ( `tests/__init__.py` ), `test_init.py` e `test_main.py`

```sh
~$ mkdir tests
~$ touch tests/__init__.py
~$ touch tests/test_init.py
~$ touch tests/test_main.py
```
---

<!-- embedme src/main/python/example-project/tests/test_init.py -->

tests/test_init.py

```py
from unittest import TestCase
from unittest.mock import MagicMock

class InitTest(TestCase):

    def test_import(self):
        import example_package

    def test_output(self):
        import example_package
        method = MagicMock()
        example_package.main(method)
        method.assert_called_once_with(
            example_package.__name__,
            example_package.__version__,
        )
```

---

<!-- embedme src/main/python/example-project/tests/test_main.py -->

tests/test_main.py

```py
from unittest import TestCase


class MainTest(TestCase):

    def test_import(self):
        import example_package.__main__
```

---

# Rodando os testes:

Pipfile:


```toml
[scripts]
test = "coverage run --source=./ -m unittest discover"
coverage-report = "coverage report"
coverage-html-report = "coverage html"
```

```sh
~$ pipenv run test
~$ pipenv run coverage-report
~$ pipenv run coverage-html-report
~$ google-chrome htmlcov/index.html
```

---

<!-- header: Projeto: Pacote -->

# Preparando o Pacote

Criar `README.md` e `setup.py`

```sh
~$ touch README.md
~$ touch setup.py
```

---

<!-- embedme  src/main/python/example-project/README.md -->

README.md

Descrição "completa" do projeto

```md
# Example Project/Package

Just a sample project to show how to publish a package in PyPI.

FLISol 2021 - 10h30 24/04/2021

https://www.univem.edu.br/eventos/flisol-2021

https://www.youtube.com/channel/UCfldRjToAJ2EzQzr9Ss1Oiw
```
---

<!-- embedme  src/main/python/example-project/setup.py -->

setup.py

```py
from setuptools import setup, find_packages

from example_package import __version__


def read_file(file_name):
    with open(file_name) as input_stream:
        return input_stream.read()


setup(
    name='flisol-example-package',
    version=__version__,
    description='FLISol Example Package',
    long_description=read_file('README.md'),
    long_description_content_type='text/markdown',
    url='https://gitlab.com/ettoreleandrotognoli/pypi-flisol',
    author='Ettore Leandro Tognoli',
    author_email='ettoreleandrotognoli@gmail.com',
    license='MIT',
    classifiers=[
        'License :: OSI Approved :: MIT License',
    ],
    packages=find_packages(exclude=('tests',)),
)

```

---

# Criando o pacote

Pipfile:

```toml
[scripts]
build = "python setup.py sdist"
```

```sh
~$ pipenv run build
```

Novas pastas:

- build
- dist
- flisol_example_package.egg-info
---

# Estrutura final:

- example-project
    - Pipfile
    - Pipfile.lock
    - README.md
    - setup.py
    - example_package
        - \_\_init__.py
        - \_\_main__.py

---


<!-- header: Projeto: Publicação -->

# Publicação de teste

Pipfile:

```toml
[scripts]
test-publish = "twine upload --repository testpypi dist/*"
```

```sh
~$ pipenv run test-publish
```

<!-- _footer: https://test.pypi.org/ -->

---

# Publicação oficial

Pipfile:

```toml
[scripts]
publish = "twine upload dist/*"
```

```sh
~$ pipenv run publish
```

<!-- _footer: https://pypi.org/ -->

---

<!-- _header: {{ CI_TIMESTAMP }} - {{ CI_COMMIT_REF_NAME }}#{{ CI_COMMIT_SHA }} -->
<!-- paginate: false -->
<!-- _class: center invert cover -->

![](resources/flisol.svg)
# Obrigado!