CONTAINER?=docker
PWD := $(shell pwd)
CONTAINER_PARAMS := --rm --user $(shell id --user) -w ${PWD} -v ${PWD}:${PWD}
PLANTUML_SVG := $(patsubst src/main/plantuml/%,src/main/resources/%,$(patsubst %.puml,%.svg,$(shell find src/main/plantuml/ -name "*.puml")))
MARP_PDF := $(patsubst src/main/marp/%,target/%,$(patsubst %.md,%.pdf,$(shell find src/main/marp/ -name "*.md")))
MARP_HTML := $(patsubst src/main/marp/%,target/%,$(patsubst %.md,%.html,$(shell find src/main/marp/ -name "*.md")))
MARP := ${CONTAINER} run ${CONTAINER_PARAMS} ciag/marp-cli marp --allow-local-files
PLANTUML := ${CONTAINER} run ${CONTAINER_PARAMS} miy4/plantuml:8053
CI_COMMIT_REF_NAME := $(shell git symbolic-ref -q --short HEAD || git describe --tags --exact-match)
CI_COMMIT_SHA=$(shell git rev-parse HEAD)
CI_TIMESTAMP=$(shell date --iso-8601=seconds)

define interpolate
	mkdir -p $(shell dirname $2)
	cp $1 $2
	sed -i "s/{{ CI_COMMIT_REF_NAME }}/${CI_COMMIT_REF_NAME}/" $2
	sed -i "s/{{ CI_COMMIT_SHA }}/${CI_COMMIT_SHA}/" $2
	sed -i "s/{{ CI_TIMESTAMP }}/"${CI_TIMESTAMP}"/" $2
endef

all: $(MARP_PDF) $(MARP_HTML) $(PLANTUML_SVG)

html: $(MARP_HTML)

pdf: $(MARP_PDF)


plantuml: $(PLANTUML_SVG)

target/resources: src/main/resources
	mkdir -p target/resources
	mkdir -p target/main/marp/
	cp -r src/main/resources/* target/resources
	ln -s ${PWD}/target/resources ${PWD}/target/main/marp/resources

target/main/marp/%.md: src/main/marp/%.md
	npm run build-one -- $<
	$(call interpolate,$<,$@)

target/%.pdf: target/main/marp/%.md $(PLANTUML_SVG) target/resources
	${MARP} $< -o $@

target/%.html: target/main/marp/%.md $(PLANTUML_SVG) target/resources
	${MARP} $< -o $@

src/main/resources/%.svg: src/main/plantuml/%.puml
	echo $^ $@
	${PLANTUML} $^ -tsvg -charset utf-8 -o ${PWD}/$(shell dirname $@)

clean:
	rm -rf target $(PLANTUML_SVG) $(MARP_HTML)